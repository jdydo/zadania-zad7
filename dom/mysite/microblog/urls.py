from django.contrib.auth.decorators import login_required
from django.conf.urls import patterns, url
from views import *

urlpatterns = patterns('',
    url(r'^$', AllPosts.as_view(), name='all_posts'),
    url(r'^user/$', ChooseUser.as_view(), name='choose_user'),
    url(r'^add/$', login_required(AddPost.as_view()), name='add_post'),
    url(r'^login/$', Login.as_view(), name='login'),
    url(r'^logout/$', login_required(Logout.as_view()), name='logout'),
    url(r'^user/(?P<user_id>\d+)/$', UserPosts.as_view(), name='user_posts'),
    url(r'^reset/$', Reset.as_view()),
)