from django.contrib.auth.models import User
from models import Posts
from django.utils import timezone
from sample_content import *


def delete_all():
    Posts.objects.all().delete()
    User.objects.all().delete()


def add_sample_content():
    user1 = User.objects.create_user(
        username='Shepard', password='Shepard', email='shepard@citadel.com', first_name='Commander', last_name='Shepard'
    )
    user2 = User.objects.create_user(
        username='Miranda', password='Miranda', email='miranda@citadel.com', first_name='Miranda', last_name='Lawson'
    )
    user3 = User.objects.create_user(
        username='Urdnot', password='Urdnot', email='urdnot@citadel.com', first_name='Urdnot', last_name='Wrex'
    )

    user1.save()
    user2.save()
    user3.save()

    Posts(author=user1, text=CONTENT_1, title='Episode I', date=timezone.now()).save()
    Posts(author=user1, text=CONTENT_2, title='Episode II', date=timezone.now()).save()
    Posts(author=user1, text=CONTENT_3, title='Episode III', date=timezone.now()).save()
    Posts(author=user2, text=CONTENT_4, title='Episode IV', date=timezone.now()).save()
    Posts(author=user2, text=CONTENT_5, title='Episode V', date=timezone.now()).save()
    Posts(author=user3, text=CONTENT_6, title='Episode VI', date=timezone.now()).save()
    Posts(author=user3, text=CONTENT_7, title='Episode VII', date=timezone.now()).save()