# -*- coding=utf-8 -*-

CONTENT_1 = '''
    David Anderson didn’t have any children of his own, and had the matter been left to him, the ex-­navy officer would
    have ordered the teenager out of the apartment with possibly unpleasant results. Fortunately, the woman he loved
    knew how to deal with such situations. Kahlee was in good shape for a woman in her forties, or thirties for that
    matter. As she smiled tiny creases appeared around her eyes. “You can’t stay here because David and I may want you
    to tell the Council what happened on the day Grayson invaded the Grissom Academy. It’s important to make sure that
    nothing like that ever happens again.

    ”Nick had been shot in the stomach during the attack and sent to the Citadel for advanced medical treatment.
    So he knew about Grayson firsthand. Nick, with shoulder-­length black hair and a relatively small frame for a
    boy his age, looked hopeful. “Can I go to The Cube on the way back?”

    “Sure,” Kahlee replied. “But only for an hour. Come on—­let’s go.”
'''

CONTENT_2 = '''
    A crisis had been averted, and Anderson was grateful. As they left the apartment the door locked behind them.
    An elevator took them down to the first floor and out into the hectic crush of the lower wards. A monorail loomed
    overhead, the pedways were crowded with individuals of every species, and the streets were jammed with ground
    vehicles. All of which was normal for the huge star-­shaped space station that served as the cultural, financial,
    and political hub of the ­galaxy.

    A crisis had been averted, and Anderson was grateful. As they left the apartment the door locked behind them.
    An elevator took them down to the first floor and out into the hectic crush of the lower wards. A monorail loomed
    overhead, the pedways were crowded with individuals of every species, and the streets were jammed with ground
    vehicles. All of which was normal for the huge star-­shaped space station that served as the cultural, financial,
    and political hub of the ­galaxy.
'''

CONTENT_3 = '''
    Anderson had been an admiral, and the Alliance’s representative to the Citadel Council, so he had spent a lot of
    time aboard the habitat. Everything was organized around a central ring. It was ten kilometers across, and the
    Citadel’s forty-­kilometer-­long “fingers” pointed from it to the stars beyond. The total population of the station
    was said to be in excess of thirteen million sentients, none of whom had played a role in creating the complex
    structure.

    The asari had discovered the station 2,700 years earlier while exploring the vast network of mass relays put in
    place by a space-­faring species known as the protheans. Having established a base on the Citadel, the asari
    learned how to create mass effect fields, and made use of them to explore the galaxy.
'''

CONTENT_4 = '''
    When the salarians found the space station a few decades later the two races agreed to form the Citadel Council for
    the purpose of settling disputes. And as more species began to travel the stars, they had little choice but to
    follow the dictates of the technologically advanced Council races. Humans were relative newcomers and had only
    recently been granted a seat on the Citadel Council.

    For many years it had been assumed that the protheans were responsible for creating the Citadel. But more recently
    it had been learned that the real architects were a mysterious race of sentient star- ships called the Reapers who
    conceived of the space station as a trap, and were responsible for annihilating all organic sentients every fifty
    thousand years or so. And, even though Reapers were trapped in dark space, there was evidence that they could reach
    out and control their servants from light-­years away. And that, Anderson believed, was a continuing threat. One the
    Council should deal with immediately.
'''

CONTENT_5 = '''
    The problem being that day-­to-­day interspecies rivalries often got in the way of the big picture. That was just
     one of the reasons why it had been so difficult for Anderson and Kahlee to get the Council to look beyond
     historical grievances to the greater threat represented by the Reapers. Anderson and Kahlee were certain that the
     Reapers had been in at least partial control of Grayson when he invaded the Grissom Academy, but they were still
     struggling to convince certain members of the Council. And that had everything to do with the presentation they
     planned to give. Hopefully, if they were successful, the Council would agree to unify behind an effort to counter
     the danger that threatened them all. Otherwise the Reapers would do what they had done before—­wipe the galaxy
     clean of sentient life.

    As Anderson led the others aboard a public shuttle he was reminded of the fact that the Reapers had created the
    Citadel as bait for a high-­tech trap. One that had been sprung so successfully that now, two years later,
    some of the damage the sentient machines had caused was still being repaired.
'''

CONTENT_6 = '''
    The vehicle came to life as Anderson settled himself behind the controls. The contragravity speeder was powered by
    a mass effect field and would carry them from the lower wards to the vicinity of the Presidium where the Council’s
    offices were located. Kahlee was sitting next to him and Nick was in the back, fiddling with his omni-­tool.
    The device consisted of an orange hologram that was superimposed over the teenager’s right arm. It could be used for
    hacking computers, repairing electronic devices, and playing games. And that’s what Nick was doing as Anderson
    guided the shuttle through a maze of streets, under graceful pedways, and into the flow of traffic that ran
    like a river between a pair of high-­rise cliffs.

    Ten minutes later the shuttle pulled into a rapid-­transit platform where they disembarked. A short, tubby volus
    pushed his way forward to claim the speedster for himself. He was dressed in an environment suit and most of his
    face was hidden by a breathing mask. “Make way Earth people—­I don’t have all day.”
'''

CONTENT_7 = '''
    They were accustomed to the often rude manner in which the Citadel’s citizens interacted with each other and
    weren’t surprised by the stranger’s contentious tone. The volus were closely allied with the raptorlike
    turians—­many of whom still felt a degree of animosity toward humans resulting from the First Contact War.
    And that was just one of the problems which prevented the races from trusting each other.

    As Anderson, Kahlee, and Nick walked toward a bank of elevators they passed a pair of beautiful asari.
    The species were asexual, but to Anderson’s eye they looked like human females, even if their skin had a bluish
    tint. Rather than hair, waves of sculpted skin could be seen on the backs of their heads and they were very shapely.
    “You can put your eyeballs back in your head now,” Kahlee commented as they entered the elevator. “No wonder the
    asari get along without men. Maybe I could too.”
'''

