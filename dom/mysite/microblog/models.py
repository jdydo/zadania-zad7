from django.contrib.auth.models import User
from django.db import models


class Posts(models.Model):
    author = models.ForeignKey(User)
    title = models.CharField(max_length=200)
    text = models.TextField()
    date = models.DateTimeField()


