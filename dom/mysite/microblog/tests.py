# -*- coding=utf-8 -*-

from django.test import TestCase
from django.test.client import Client
from django.test.utils import setup_test_environment
from models import Posts
from django.contrib.auth.models import User
from django.utils import timezone


setup_test_environment()
client = Client()


class MicroblogTest(TestCase):
    def extend_html_check(self, response, logged):
        if logged:
            assert 'All Posts' in response.content
            assert 'User Posts' in response.content
            assert 'Add Post' in response.content
            assert 'Log out' in response.content
            assert 'Log in' not in response.content
        else:
            assert 'All Posts' in response.content
            assert 'User Posts' in response.content
            assert 'Add Post' not in response.content
            assert 'Log out' not in response.content
            assert 'Log in' in response.content

    def create_user1(self):
        return User.objects.create_user(
            username='Shepard', password='Shepard', email='shepard@citadel.com', first_name='Commander',
            last_name='Shepard'
        )

    def create_user2(self):
        return User.objects.create_user(
            username='Miranda', password='Miranda', email='miranda@citadel.com', first_name='Miranda',
            last_name='Lawson'
        )

    def test_empty_user_base(self):
        assert User.objects.count() == 0

    def test_empty_post_base(self):
        assert Posts.objects.count() == 0

    def test_filled_user_base(self):
        User.objects.create_user(
            username='Shepard', password='Shepard', email='shepard@citadel.com', first_name='Commander',
            last_name='Shepard'
        ).save()
        assert User.objects.count() == 1

    def test_filled_post_base(self):
        user = self.create_user1()
        user.save()
        Posts(author=user, text='Some Text', title='Some Title', date=timezone.now()).save()
        assert Posts.objects.count() == 1

    def test_model(self):
        date = timezone.now()
        user = self.create_user1()
        user.save()
        Posts(author=user, text='Some Text', title='Some Title', date=date).save()
        post = Posts.objects.filter(id=1)[0]
        assert 'Shepard' == post.author.username
        assert 'Some Title' == post.title
        assert 'Some Text' == post.text
        assert date == post.date

    def test_404a(self):
        assert client.get('/addaaa').status_code == 404

    def test_get_empty_blog(self):
        response = client.get('/')
        assert response.status_code == 200
        assert len(response.context['posts']) == 0
        self.extend_html_check(response, False)

    def test_get_filled_blog(self):
        date = timezone.now()
        user = self.create_user1()
        user.save()
        Posts(author=user, text='Some Text', title='Some Title', date=date).save()
        response = client.get('/')
        assert response.status_code == 200
        assert len(response.context['posts']) == 1
        self.extend_html_check(response, False)

    def test_get_user_list(self):
        self.create_user1().save()
        self.create_user2().save()
        response = client.get('/user/')
        assert response.status_code == 200
        assert len(response.context['users']) == 2
        assert 'Miranda - Miranda Lawson' in response.content
        assert 'Shepard - Commander Shepard' in response.content
        self.extend_html_check(response, False)

    def test_get_user_posts(self):
        user1 = self.create_user1()
        user2 = self.create_user2()
        user1.save()
        user2.save()
        Posts(author=user1, text='Some Text1', title='Some Title1', date=timezone.now()).save()
        Posts(author=user2, text='Some Text2', title='Some Title2', date=timezone.now()).save()
        response = client.get('/user/1/')
        assert response.status_code == 200
        assert 'Commander Shepard' in response.content
        assert 'Some Text1' in response.content
        assert 'Some Title1' in response.content
        assert 'Miranda Lawson' not in response.content
        assert 'Some Text2' not in response.content
        assert 'Some Title2' not in response.content
        self.extend_html_check(response, False)

    def test_404b(self):
        assert client.get('/user/7/').status_code == 404

    def test_get_login(self):
        response = client.get('/login/')
        assert response.status_code == 200
        self.extend_html_check(response, False)
        assert 'name="username"' in response.content
        assert 'name="password"' in response.content

    def test_post_login_with_valid_data(self):
        self.create_user1().save()
        response = client.post('/login/', {'username': 'Shepard', 'password': 'Shepard'}, follow=True)
        assert response.status_code == 200
        assert 'Hello, Commander Shepard!' in response.content
        self.extend_html_check(response, True)
        assert 'Logged in!' in response.content

    def test_post_login_with_invalid_data(self):
        self.create_user1().save()
        response = client.post('/login/', {'username': 'bad', 'password': 'bad'}, follow=True)
        assert response.status_code == 200
        assert 'Error! Wrong username or password.' in response.content
        self.extend_html_check(response, False)
        assert 'name="username"' in response.content
        assert 'name="password"' in response.content

    def test_get_add_without_login(self):
        response = client.get('/add/', follow=True)
        assert response.status_code == 200
        self.extend_html_check(response, False)
        assert 'name="username"' in response.content
        assert 'name="password"' in response.content

    def test_get_add_with_login(self):
        self.create_user1().save()
        client.post('/login/', {'username': 'Shepard', 'password': 'Shepard'})
        response = client.get('/add/')
        assert response.status_code == 200
        assert 'Hello, Commander Shepard!' in response.content
        self.extend_html_check(response, True)
        assert 'name="title"' in response.content
        assert 'name="text"' in response.content

    def test_post_add_without_login(self):
        response = client.post('/add/', {'title': 'Some Title', 'text': 'Some Text'}, follow=True)
        assert response.status_code == 200
        self.extend_html_check(response, False)
        assert 'name="username"' in response.content
        assert 'name="password"' in response.content

    def test_post_add_with_login_and_valid_data(self):
        self.create_user1().save()
        client.post('/login/', {'username': 'Shepard', 'password': 'Shepard'})
        response = client.post('/add/', {'title': 'Some Title', 'text': 'Some Text'}, follow=True)
        assert response.status_code == 200
        assert 'Post has been added!' in response.content
        response = client.get('/')
        assert len(response.context['posts']) == 1
        assert 'Hello, Commander Shepard!' in response.content
        self.extend_html_check(response, True)
        assert 'Some Text' in response.content
        assert 'Some Title' in response.content

    def test_post_add_with_login_and_invalid_data(self):
        self.create_user1().save()
        client.post('/login/', {'username': 'Shepard', 'password': 'Shepard'})
        response = client.post('/add/', {'title': '', 'text': ''}, follow=True)
        assert response.status_code == 200
        assert 'Error! Title and text must be filled.' in response.content
        assert 'Hello, Commander Shepard!' in response.content
        self.extend_html_check(response, True)
        assert 'name="title"' in response.content
        assert 'name="text"' in response.content

    def test_get_logout(self):
        self.create_user1().save()
        client.post('/login/', {'username': 'Shepard', 'password': 'Shepard'})
        response = client.get('/logout/', follow=True)
        assert response.status_code == 200
        assert 'Hello, Commander Shepard!' not in response.content
        self.extend_html_check(response, False)
        assert 'Logged out!' in response.content
