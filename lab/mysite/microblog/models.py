from django.db import models


class Posts(models.Model):
    author = models.CharField(max_length=50)
    text = models.TextField()
    date = models.DateTimeField()