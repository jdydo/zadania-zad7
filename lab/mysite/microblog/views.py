from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.views.generic import View
from django.shortcuts import render
from django.utils import timezone
from models import Posts


class Blog(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'microblog/blog.html', {'posts': Posts.objects.all().order_by('-id')})


class Add(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'microblog/add.html')

    def post(self, request, *args, **kwargs):
        Posts(author=request.POST['author'], text=request.POST['text'], date=timezone.now()).save()
        return HttpResponseRedirect(reverse('blog'))