from django.test import TestCase
from django.test.client import Client
from django.test.utils import setup_test_environment
from models import Posts
from django.utils import timezone


setup_test_environment()
client = Client()


class MicroblogTest(TestCase):

    def test_empty_base(self):
        assert Posts.objects.count() == 0

    def test_filled_base(self):
        Posts(author='Jan', text='Lorem Ipsum', date=timezone.now()).save()
        assert Posts.objects.count() == 1

    def test_model(self):
        date = timezone.now()
        Posts(author='Jan', text='Lorem Ipsum', date=date).save()
        post = Posts.objects.filter(id=1)[0]
        assert 'Jan' == post.author
        assert 'Lorem Ipsum' == post.text
        assert date == post.date

    def test_empty_blog_get(self):
        response = client.get('/blog/')
        assert response.status_code == 200
        assert len(response.context['posts']) == 0
        assert 'Home' in response.content
        assert 'Add' in response.content

    def test_filled_blog_get(self):
        date = timezone.now()
        Posts(author='Jan', text='Lorem Ipsum', date=date).save()
        response = client.get('/blog/')
        assert response.status_code == 200
        assert len(response.context['posts']) == 1
        assert 'Home' in response.content
        assert 'Add' in response.content
        assert 'Jan' in response.content
        assert 'Lorem Ipsum' in response.content

    def test_add_get(self):
        response = client.get('/blog/add/')
        assert response.status_code == 200
        assert 'Home' in response.content
        assert 'Add' in response.content
        assert 'name="author"' in response.content
        assert 'name="text"' in response.content

    def test_add_post(self):
        response = client.get('/blog/add/')
        assert 'Home' in response.content
        assert 'Add' in response.content
        assert 'Jan' not in response.content
        assert 'Lorem Ipsum' not in response.content

        response = client.post('/blog/add/', {'author': 'Jan', 'text': 'Lorem Ipsum'}, follow=True)
        assert response.status_code == 200
        assert len(response.context['posts']) == 1
        assert 'Home' in response.content
        assert 'Add' in response.content
        assert 'Jan' in response.content
        assert 'Lorem Ipsum' in response.content
