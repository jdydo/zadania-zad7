from django.conf.urls import patterns, url
from django.views.generic import TemplateView
from views import *

urlpatterns = patterns('',
    url(r'^$', Blog.as_view(), name='blog'),
    url(r'^add/$', Add.as_view(), name='add')
)